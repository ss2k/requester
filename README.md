## Assistance Requests

# Background

This is a simple front end application that works with a backend API to submit requests for assistance.
The application does the following things:
- Takes in input and checks if the input is filled in or not
- Submits it to backend api
- Responds with a success or a failure message
- Resets form if the request is successful

# Instructions
To install just run ```npm install``` and then ```npm start```. 
You also need to be running the ```fake_api``` software as well.

# Dependencies
The major external dependencies are as follows:
- TailwindCSS - Utility based CSS frame work
- Axios - HTTP client