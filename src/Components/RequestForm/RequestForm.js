import React, { useState, useEffect } from 'react';
import axios from 'axios';

const API_BASE = 'http://localhost:49567/api/'

const RequestForm = () => {

  // Set initial states
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [chosenService, setChosenService] = useState('');
  const [description, setDescription] = useState('');

  const [serviceTypes, setServiceTypes] = useState([]);

  // Get all the service types via API call
  async function getServiceTypes() {
    axios.get(API_BASE + 'service-types')
         .then(res => {
          setServiceTypes(res.data.data);
         });
  }
 
  // Reset form
  const resetForm = () => {
    setFirstName('');
    setLastName('');
    setEmailAddress('');
    setChosenService('');
    setDescription('');
  }

  // Change background color of the form card
  const changeFormBackgroundColor = (className) => {
    document.getElementById('request_card').classList.add(className);
    setTimeout(function() {
      document.getElementById('request_card').classList.remove(className);
    }, 3000)
  }

  // Alert for form submission result
  const formAlert = (result) => {
    if (result == 'success') {
      changeFormBackgroundColor('bg-green-300');
    } else if (result == 'error') {
      changeFormBackgroundColor('bg-red-300');
    }
  }

  // Submit Form
  const submitForm = (e) => {
    e.preventDefault();

    axios({
      method: 'post',
      url: API_BASE + 'assistance-requests',
      headers: { 'Content-Type': 'application/json'},
      data: {
        assistance_request: {
          contact: {
            first_name: firstName,
            last_name: lastName,
            email: emailAddress
          },
          service_type: chosenService,
          description: description
        }
      }
    }).then(function(res) {
      formAlert('success');
      alert("You have successfully submitted a new request");
      resetForm();
    })
    .catch(function(error) {
      formAlert('error');
      alert(error.message);
    });
  }

  // Get service types only once
  useEffect(() => {
    getServiceTypes();  
  }, []);

  return(
    <div className='container p-4 shadow-lg mx-auto my-8 border max-w-lg' id='request_card'>
      <div className='border-b-2 border-black p-2'>
        New Assistance Request
      </div>
      <div className='my-4'>
        <form onSubmit={submitForm}>
          <div>
            <input type="text" placeholder="First Name" value={firstName}
            onChange = {(e) => setFirstName(e.target.value) }
            className="border-black border-2 w-full p-2 hover:bg-blue-100" required />
          </div>
          <div className='my-4'>
            <input type="text" placeholder="Last Name" value={lastName}
            onChange = {(e) => setLastName(e.target.value) }
            className="border-black border-2 w-full p-2 hover:bg-blue-100" required />
          </div>
          <div className='my-4'>
            <input type="email" placeholder="Email Address" value={emailAddress}
            onChange = {(e) => setEmailAddress(e.target.value) }
            className="border-black border-2 w-full p-2 hover:bg-blue-100" required />
          </div>
          <div className='my-4'>
            <select 
            onChange = {(e) => setChosenService(e.target.value)}
            className="border-black border-2 w-full p-2 hover:bg-blue-100" required>
              <option value="">Select Service Type</option>
              {
                serviceTypes.map(types => 
                  <option key={types.id} value={types.id}> {types.display_name} </option>
                )
              }
            </select>
          </div>
          <div className="my-4">
            <textarea value={description}
            onChange = {(e) => setDescription(e.target.value)}
            className="border-black border-2 w-full p-2 hover:bg-blue-100" required>
            </textarea>
          </div>
          <div className="my-4">
            <input type="checkbox" required checked readOnly /> I hereby accept the terms of service for THE NETWORK and the Privacy Policy
          </div>
          <div className="my-4 flex flex-col">
            <button type="submit" className='shadow-2xl bg-blue-300 p-4 text-white font-bold hover:bg-blue-400'>Get Assistance</button>
          </div>
        </form>
      </div>
    </div>
  );
}

export default RequestForm